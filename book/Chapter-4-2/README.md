####تشخیص و تبدیل انواع داده در پایتون (۲)

در جلسه ی قبل با تابع <span class = "code"> ()type</span> آشنا شدیم و با توابع <span class = "code"> ()int</span> ، <span class = "code"> float</span> انواعی از داده رو به هم تبدیل کردیم. همین طور متوجه شدیم که تبدیل داده های بولین  <span class = "code"> True</span> و <span class = "code"> False</span> به عدد صحیح، خروجی  <span class = "code"> 1</span> و <span class = "code"> 0</span> میده. در ادامه ی این بحث، با تابع  <span class = "code"> ()bool</span> آشنا و انواع داده رو، به بولین تبدیل می کنیم. با مثال پیش میریم:

```python 
print(bool('Hello'))
print(bool(27))
print(bool(9.97))
print(bool(['dog']))
print(bool(('cat')))
print(bool({'age': 25}))
print(bool({47}))
```

خروجی تمام کدهای بالا، یعنی رشته ای که خالی نیست، عدد صحیح و اعشاری که صفر نیست و لیست، تاپل، دیکشنری و <span class = "code"> set</span> که دارای عضو هستن، <span class = "code">True </span> است. اما به این نمونه ها دقت کنید:

```python
print(bool(''))
print(bool(0))
print(bool(0.0))
print(bool([]))
print(bool(()))
print(bool({}))
print(bool(None))
```

به صورت کلی تبدیل یک رشته با طول صفر <span class = "note"> (خالی)</span>، اعداد صحیح و اعشاری صفر، لیست، تاپل، دیکشنری و set و همچنین داده ی <span class = "code"> None</span> به بولین، <span class = "code"> False</span> رو برمیگردونه. <span class = "code"> None</span> یک نوع داده ی استاندارد در پایتون هست که امروز باهاش آشنا میشیم. <span class = "code"> None</span> آبجکتی است که نشان دهنده ی عدم وجود یک آبجکت و تنها نمونه ای از کلاس <span class = "code"> NoneType</span> است و در سایر زبان های برنامه نویسی با <span class = "code"> Null</span> یا <span class = "code"> undefined</span> و ... نمایش داده میشود. تا همین جا شو در حافظه نگه دارید و کمی که پیش رفتیم، با نمونه های عملی آشنا میشید.

ممکنه با خودتون بپرسید اصلا «چرا باید یه رشته ی خالی رو به بولین تبدیل کنم؟»، اگر یادتون باشه قسمت اول برنامه ای رو نوشتیم که اسم کاربر رو میگرفت و بهش سلام میداد، حال اگر کاربر، نام خودشو وارد نکنه و اینتر بزنه، یه همچین اتفاقی می افته:

<pre><span class="note">
python3 hello.py
Please write your name dear user: 
Hello 
</span></pre>

اما ما میخواییم که حتما کاربر، نام خودشو وارد کنه، می تونیم بررسی کنی و بگیم، تا زمانی که کاربر، نام خودشو وارد نکرده، ازش درخواست کن که نامش رو وارد کنه و به مرحله ی بعدی نرو:

```python
username = ''

while not bool(username):
	# Assign username
	username = input("Please write your name dear user: ")

# Say Hello to your username!
print("Hello {}".format(username))
```

کد بالارو کپی و پیست و بعد اجرا کنید، همون طور که میبیند، برای اجرای ادامه ی کد، حتما نیاز هست که نام، وارد بشه. ذکر این نکته واجبه که هنوز، حلقه ها رو آموزش ندادم و لازم نیست خودتونو با کد بالا درگیر کنید، چرا که طی چند درس آینده همه ی این موارد رو یاد خواهید گرفت.

با تابع <span class = "code"> ()str</span> می تونید طیف وسیعی از انواع داده رو به رشته تبدیل کنید، برای مثال:

```python
my_data = 23
my_str= str(my_data)
print(type(my_data))
```

توابع <span class = "code"> ()list</span> و<span class = "code"> ()str</span> و <span class = "code"> ()tuple</span> بسیار شبیه هم عمل می کنند و به ترتیب، داده ها رو به لیست، set و تاپل تبدیل می کنند. این رو در نظر داشته باشید که برای تبدیل اعداد، ابتدا باید اونا رو به رشته تبدیل کنید و بعد، مجاز هستید تا از این سه تابع استفاده کنید وگرنه ارور دریافت می کنید:

```python
hurds@Asus:~$ python3
Python 3.6.3 (default, Oct  3 2017, 21:45:48) 
[GCC 7.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> number = 1397
>>> list(number)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'int' object is not iterable
>>> number = str(number)
>>> list(number)
['1', '3', '9', '7']
```

<span class = "code"> **تبدیل رشته**:</span>

```python
my_str = 'mahmood'

print(list(my_str))
print(set(my_str))
print(tuple(my_str))
```

خروجی:

```taq
['m', 'a', 'h', 'm', 'o', 'o', 'd']
{'m', 'd', 'a', 'o', 'h'}
('m', 'a', 'h', 'm', 'o', 'o', 'd')
```

همین طور میتونید خود این داده ها رو به هم تبدیل کنید، من تنها یک نمونه رو مینویسم و بقیه اش، به عنوان تمرین برعهده ی خودتونه:

```python
my_list = ['dog', 2, 2, 2, 'ice', 'python', 'dog', 'dog']
print(set(my_list))
print(tuple(my_list))
```
خروجی:
```taq
{2, 'ice', 'python', 'dog'}
('dog', 2, 2, 2, 'ice', 'python', 'dog', 'dog')
```

آخرین تبدیلی که میخواییم در این بخش یاد بگیریم، تابع <span class = "code">()dict </span> برای تبدیل به دیکشنری هست. همون طور که گفته شد، دیکشنری ها برخلاف انواع رایج داده که تنها مقدار دارند، از کلید و مقدار تشکیل شدند، به همین علت برای تشکیل دیکشنری، نیاز هست که <span class = "code">key </span> ها با <span class = "code"> value</span> ها جفت شن، یه ایده ای رو مطرح کنم، در مورد مثال آموزشگاهی که قبلا گفته شد، فرض می کنم تمام کلید ها در یک لیست، و تمام مقدارها در یک لیست دیگه هستن:

```python
keys = ['firstname', 'lastname', 'age', 'username', 'sity']
values = ['Ali', 'Moradi', 29, 'ali_moradi', 'Shiraz']
```
<span class = "code"> </span>
ابتدا به وسیله ی تابع <span class = "code">()zip </span> این دو رو به هم ربط میدم و بعد، با استفاده از <span class = "code">()dict </span>، دیکشنری خودمو تشکیل میدم:

خروجی:

```tag
{'firstname': 'Ali', 'lastname': 'Moradi', 'age': 29, 'username': 'ali_moradi', 'sity': 'Shiraz'}
```

موارد مهم و پایه ای که باید بررسی می کردیم تمام شد! قبول دارم که زیادی تئوری داریم اما برای انجام پروژه، نیاز به ابزارهایی هست که باید فرابگیریم اشون و من از پایه ای ترین سطح کار رو آغاز کردم. یه چند قسمت هم همین طور بریم جلو، به سطحی می رسیم که بتونیم با انجام پروژه های کوچیک، مواردی که یاد گرفتیم رو تثبیت کنیم و کارمون رو پیش ببریم.

فعلا خسته نباشید، خوب تمرین کنید و از آموزش بعدی، دوره رو پی بگیرید:) 


