####تشخیص و تبدیل انواع داده در پایتون (۱)

در جلسه قبل انواع داده ی استاندارد در پایتون رو با هم شناختیم، در این جلسه با استفاده از تابع <span class = "code"> ()type</span>، نوع داده رو شناسایی می کنیم و با مجموعه ای از توابعی که در پایتون تعریف شدن، داده ها رو به هم تبدیل می کنیم. مطالبی که در این درس یاد میگیرید، در نوشتن برنامه ها و پروژه ها بهتون کمک زیادی خواهند کرد.

در ابتدا یه نکته ی مربوط به تابع <span class = "code">()type </span> رو یاد بگیریم، در جلسه ی اول گفتیم که چطور می تونید با استفاده از متد <span class = "code"> ()format</span> متغیرها رو در یک رشته جایگذاری کنید، برای مثال:

```python
name = 'Hurds'
activity = 'reading'
print('{} is {} a book.'.format(name, activity))
```

خروجی:

```tag
Hurds is reading a book.
```

یک روش دیگه برای نمایش، به این صورته:

```python
name = 'Hurds'
activity = 'writing'
print(name, 'is', activity, 'a book.')
```
خروجی:

```tag
Hurds is writing a book.
```

<span class = "note"> تمرین </span>

>حالا شما بیاید نام آرمان به برنامه بدید و بهش بگید آرمان به من ns3 یاد بده ! :‌))

یعنی شما می تونید آرگومان هاتونو به تابع <span class = "code"> ()print </span> پاس بدید و خود این تابع اونارو با یه space به هم بچسبونه. البته میشه تعیین کرد که فاصله بین اشون نزاره یا به خط بعدی بره و ...، مثلا:

```python
name = 'Ali'
activity = 'buying'
print(name, 'is', activity, 'a book.', sep='\n')
```

خروجی:

```tag
Ali
is
buying
a book.
```

پارامتر <span class = "code"> sep </span> مشخص کننده ی رشته ای ایست که بین مقدارها افزوده میشه و به طور پیش فرض، فاصله است اما در کد بالا ما مشخص کردیم که به سطر بعدی بره. (با <span class = "code"> n\ </span> که در آینده مفصل یاد خواهیم گرفت). همچنین تابع <span class = "code"> ()print </span> به آخر رشته یک <span class = "code"> n\ </span> که سبب میشه رشته به خط بعدی بره، اضافه می کنه، برای مثال:

```python
print('I love python.')
print('I love python.')
print('I love python.')
```

خروجی:

```python
I love python.
I love python.
I love python.
```

اما ما با پارامتر  <span class = "code"> end </span> می تونیم مشخص کنیم که چه مقداری به آخر رشته افزوده بشه، برای مثال:

```python
print('I love python.', end=' -- ')
print('I love python.', end=' ** ')
print('I love python.')
```
خروجی:

```tag
I love python. -- I love python. ** I love python.
```

##تابع ()type

در پایتون با تابع  <span class = "code">()type </span> میشه نوع داده ای که هست رو فهمید. در کد زیر من انواع داده هایی رو مشخص کردم و با استفاده از تابع  <span class = "code">()print </span> نمایش اشون دادم:

```python
my_int = 2
my_float = 2.0
my_str = '2'
my_list = ['dog', 92, ['gnu/linux']]
my_tuple = ('football', 'basketball')
my_dict = {'name': 'Samand'}
my_bool = False
my_set = {1,2,3}

print('Type of', my_int, 'is:', type(my_int))
print('Type of', my_float, 'is:', type(my_float))
print('Type of', my_str, 'is:', type(my_str))
print('Type of', my_list, 'is:', type(my_list))
print('Type of', my_tuple, 'is:', type(my_tuple))
print('Type of', my_dict, 'is:', type(my_dict))
print('Type of', my_bool, 'is:', type(my_bool))
print('Type of', my_set, 'is:', type(my_set))
```

خروجی:

```tag
Type of 2 is: <class 'int'>
Type of 2.0 is: <class 'float'>
Type of 2 is: <class 'str'>
Type of ['dog', 92, ['gnu/linux']] is: <class 'list'>
Type of ('football', 'basketball') is: <class 'tuple'>
Type of {'name': 'Samand'} is: <class 'dict'>
Type of False is: <class 'bool'>
Type of {1, 2, 3} is: <class 'set'>
```

برای تبدیل اعداد اعشاری یا عددی که به صورت رشته اس (مثل  <span class = "code"> '23' </span>) به عدد صحیح، از تابع  <span class = "code">()int </span> استفاده می کنیم:

```python
print((int('23')))
print(int(23.3))
```
خروجی:

```tag
23
23
```

برای تبدیل عدد صحیح و یا عددی که به صورت رشته اس (مثل  <span class = "code">'9' </span>) به عدد اعشاری، از تابع  <span class = "code">()float </span> استفاده می کنیم:

```python
print((float('9')))
print(float(9))
```

خروجی:

```tag
9.0
9.0
```

نکته ی جالب خروجی تبدیل مقدارهای بولین <span class = "code">True </span>و <span class = "code">False </span> به عدد صحیح و یا عدد اعشاری است:

خروجی رو ببینیم:

```tag
1
0
1.0
0.0
```

این نکته رو به یاد داشته باشید یا در جایی یادداشت کنید، به صورت کلی عدد  <span class = "code">1 </span> در زبان های برنامه نویسی معادل <span class = "code">True </span> و عدد <span class = "code">0 </span>  معادل <span class = "code">False </span>  است.

برای این که حجم این درس زیاد بود، در دو آموزش میارم و ادامه ی این آموزش، در قسمت بعدیه. برید یه خستگی در کنید و یه بار دیگه نکات گفته شده رو مرور کنید و سعی کنید به خاطر بسپارید، یه چند جلسه هم بریم جلو و مقدمات رو یاد بگیریم، پایتون شیرین تر هم میشه و دستمون برای انجام پروژه های جالب، باز:)


