####دسترسی به اعضای لیست، تاپل، set و دیکشنری

خیلی وقت ها ممکنه فقط به یکی یا تعدادی از اعضای این نوع داده نیاز داشته باشیم. در این درس با این موضوع آشنا میشیم.

برای شروع، یک لیست از دانشجویان پایتون آماده می کنم:

```python
students = ['Mehran' ,'Sima' ,'Hasan' ,'Amir' ,'Maryam' ,'Zahra' ,'Parastoo' ,'Mahdi' ,'Ramin' ,'Fatemeh' ,'Pirooz' ,'Rahim']
```

یه نکته ای رو همین جا یاد بگیرید و همیشه به خاطر داشته باشید، پایتون، همیشه اعداد رو از «صفر» شروع به شمردن میکنه، برخلاف ماها که از یک شروع می کنیم.

>به هر عضو لیست <span class = "code">students</span>
، یک ایندکس گفته میشه، برای مثال ایندکس صفر میشه <span class = "code">Mehran</span>
 و ایندکس یک میشه <span class = "code">Sima</span>
 و همین طور تا ایندکس یازده که میشه <span class = "code">Rahim</span>
. اگر بخوام ایندکس صفر رو نشون بدم، این طوری می نویسم:

```python
print(students[0])
```
یعنی برام فقط ایندکس صفرم رو نمایش بده. خروجی:

```taq
Mehran
```


بقیه ی ایندکس ها رو هم میتونید همین طوری نمایش بدید. در نظر داشته باشید که از این نظر بین لیست و تاپل تفاوتی وجود نداره اما <span class = "code">set</span>
 از این روش پشتیبانی نمی کنه:

 ```python
 students_tuple = ('Mehran' ,'Sima' ,'Hasan' ,'Amir' ,'Maryam' ,'Zahra' ,'Parastoo' ,'Mahdi' ,'Ramin' ,'Fatemeh' ,'Pirooz' ,'Rahim')
print(students_tuple[1])

students_set = {'Mehran' ,'Sima' ,'Hasan' ,'Amir' ,'Maryam' ,'Zahra' ,'Parastoo' ,'Mahdi' ,'Ramin' ,'Fatemeh' ,'Pirooz' ,'Rahim'}
print(students_set[1])
```
خروجی:

<pre>
Sima
<span class = "code">Traceback (most recent call last):
  File "h.py", line 5, in <module>
    print(students_set[1])
TypeError: 'set' object does not support indexing</span>
</pre>

یه راه اینکه ابتدا <span class = "code">set</span> رو به لیست تبدیل و بعد، ایندکس مد نظر رو نمایش بدیم:

```python
students_set = {'Mehran' ,'Sima' ,'Hasan' ,'Amir' ,'Maryam' ,'Zahra' ,'Parastoo' ,'Mahdi' ,'Ramin' ,'Fatemeh' ,'Pirooz' ,'Rahim'}
students_list = list(students_set)
print(students_list[1])
```
ممکنه یه جایی، نیاز باشه که ایندکس صفر تا سه رو از لیست دانشجوهام جا کنم، می تونم بنویسم:

```python 
print(students[0:3])
```
خروجی:

```tag
['Mehran', 'Sima', 'Hasan']
```
در صورتی که بخوام استارت از صفر باشه، می تونم اون گزینه رو خالی هم بزارم:

```python
print(students[:3])
```
خروجی این کد با کد قبلی، تفاوتی نداره.

یه جایی هست که میخوام ایندکس ششم تا آخر رو نمایش بدم، می تونم بنویسم:

```python
print(students[6:12])
```
و یا:
```python
print(students[6:])
```
خروجی:

```tag
['Parastoo', 'Mahdi', 'Ramin', 'Fatemeh', 'Pirooz', 'Rahim']
```

گاهی وقت ها هم، میخوام که یک در میان ایندکس ها رو نمایش بدم، می تونم در این مثال بنویسم:

```python
print(students[::2])
```
خروجی:

```tag
['Mehran', 'Hasan', 'Maryam', 'Parastoo', 'Ramin', 'Pirooz']
```
همین طور میتونم <span class = "code">n</span> تا <span class = "code">n</span> تا جدا کنم، به تفاوت خروجی دستورات زیر دقت کنید:

```python
print(students[0::3])
print(students[1::3])
print(students[2::3])
```
خروجی:

```tag
['Mehran', 'Amir', 'Parastoo', 'Fatemeh']
['Sima', 'Maryam', 'Mahdi', 'Pirooz']
['Hasan', 'Zahra', 'Ramin', 'Rahim']
```
کد اول، از ایندکس صفر شروع کرده و بعد، ایندکس های سه و شش و نه رو نمایش داده، کد دوم از ایندکس یک شروع، و ایندکس های چهار و هفت و ده رو نمایش داده، کد سوم هم از ایندکس دو آغاز و ایندکس های پنج و هشت و یازده رو نمایش داده.

یه تکنیک خوب دیگه اینکه میخواییم ایندکس ها رو از آخر بشماریم اما شماره ی ایندکس آخر رو نمی دونیم. می تونیم بنویسیم:

```python
print(students[-1])
print(students[-2])
print(students[-3])
```
خروجی:
```tag
Rahim
Pirooz
Fatemeh
```
یعنی ایندکس آخر میشه <span class = "code">1-</span>، ایندکس ماقبل آخر <span class = "code">2-</span> و همین طور میشه نوشت.

تا اینجا نمایش اعضای لیست و تاپل و <span class = "code">set</span> رو انجام دادیم و تنها، دیکشنری ها موندن. با یه مثال پیش بریم:

```python
Rahim = {
	'firstname': 'Rahim',
	'lastname': 'Karimi',
	'age': 29,
	'favourites': ['coffee', 'python', 'football'],
}
```
میتونیم با استفاده از کلید، به هر مقدار دست پیدا کنیم:

```python
print(Rahim['firstname'])
print(Rahim['lastname'])
print(Rahim['age'])
print(Rahim['favourites'])
```
خروجی:

```tag
Rahim
Karimi
29
['coffee', 'python', 'football']
```
همون طور که میبینید، علایق رحیم، خود در یه لیست دیگه قرار میگیره، می تونیم به هر کدوم هم جداگانه دسترسی داشته باشیم:
```python
print(Rahim['favourites'][0])
print(Rahim['favourites'][1])
print(Rahim['favourites'][2])
```
خروجی:

```tag
coffee
python
football
```
درس این جلسه هم همین جا به پایان میرسه. از جلسه ی آینده دستورات شرطی در پایتون رو بررسی می کنیم و چند درس آینده، مهارت خودمون رو تا جایی افزایش میدیم که بتونیم پروزه های کوچیک کد بزنیم. پس دوره رو حتما با جدیت ادامه بدید:)



